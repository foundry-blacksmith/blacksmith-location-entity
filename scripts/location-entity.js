import {LocationToken, LocationTokenHelpers} from "./location-token.js";

/**
 * Location Entity type to manage locations independently from scenes.
 */
export class Location extends Entity {
    constructor(...args) {
        super(...args);

        /**
         * A reference to a placed Token which creates a synthetic Location
         * @type {LocationToken}
         */
        this.token = this.options.token || null;

        /**
         * Construct the Array of Location instances for the Location
         * Inner Locations are prepared by the Location.prepareEmbeddedEntities() method
         * @type {Collection<string,InnerLocation>}
         */
        this.inner_locations = [];

        /**
         * Cache an Array of allowed Token images if using a wildcard path
         * @type {Array}
         * @private
         */
        this._tokenImages = null;
    }

    /** @override */
    static get config() {
        return {
            baseEntity: Location,
            collection: game.locations,
            embeddedEntities: {"InnerLocations": "inner_locations"},
            label: "ENTITY.Location",
            permissions: {
                create: "LOCATION_CREATE"
            }
        };
    }

    /** @override */
    prepareData() {
        super.prepareData();
        if (!this.data.img) this.data.img = CONST.DEFAULT_TOKEN;
    }

    /** @override */
    prepareEmbeddedEntities() {
        const prior = this.inner_locations;
        const locations = new Collection();
        for (let i of this.data.inner_locations) {
            let loc = null;

            // Update existing items
            if (prior && prior.has(i._id)) {
                loc = prior.get(i._id);
                loc.data = i;
                loc.prepareData();
            }

            // Construct new items
            else loc = Location.createInnerLocation(i, this);
            locations.set(i._id, loc);
        }

        // Assign Items to the Location
        this.inner_locations = locations;
    }


    /* -------------------------------------------- */
    /*  Properties                                  */

    /* -------------------------------------------- */

    /**
     * A convenient reference to the file path of the Location's profile image
     * @type {string}
     */
    get img() {
        return this.data.img;
    }

    /* -------------------------------------------- */

    /**
     * Classify Inner Locations by their type
     * @type {Object<string,Array>}
     */
    get locationTypes() {
        const types = Object.fromEntries(game.system.entityTypes.Location.map(t => [t, []]));
        for (let i of this.inner_locations.values()) {
            types[i.data.type].push(i);
        }
        return types;
    }

    /* -------------------------------------------- */

    /**
     * Test whether an Location entity is a synthetic representation of a Token (if true) or a full Entity (if false)
     * @type {boolean}
     */
    get isToken() {
        if (!this.token) return false;
        return !this.token.data.locationLink;    // TODO: Adapt Token to allow Location Tokens
    }

    /* -------------------------------------------- */
    /*  Methods                                     */

    /* -------------------------------------------- */

    /**
     * Create a synthetic Location using a provided Token instance
     * If the Token data is linked, return the true Location entity
     * If the Token data is not linked, create a synthetic Location using the Token's locationData override
     * @param {LocationToken} token
     * @return {Location}
     */
    static fromToken(token) {
        let location = game.locations.get(token.data.locationId);
        if (!location) return null;
        if (!token.data._id) return location;
        if (!token.data.locationLink) location = location.constructor.createTokenLocation(location, token);
        return location;
    }

    /* -------------------------------------------- */

    /**
     * Create a synthetic Token Location instance which is used in place of an actual Location.
     * Cache the result in Locations.tokens.
     * @param {Location} baseLocation
     * @param {LocationToken} token
     * @return {Location}
     */
    static createTokenLocation(baseLocation, token) {
        let location = this.collection.tokens[token.id];
        if (location) return location;
        const locationData = mergeObject(baseLocation.data, token.data.locationData, {inplace: false});
        location = new this(locationData, {token: token});
        return this.collection.tokens[token.id] = location;
    }

    /* -------------------------------------------- */

    /**
     * Retrieve an Array of active tokens which represent this Location in the current canvas Scene.
     * If the canvas is not currently active, or there are no linked locations, the returned Array will be empty.
     *
     * @param [linked] {Boolean}  Only return tokens which are linked to the Location. Default (false) is to return all
     *                            tokens even those which are not linked.
     *
     * @return {Array}            An array of tokens in the current Scene which reference this Location.
     */
    getActiveTokens(linked = false) {
        if (!canvas.tokens) return [];
        return canvas.tokens.placeables.filter(t => {
            if (!(t instanceof LocationToken)) return false;
            if (linked) return t.data.locationLink && t.data.locationId === this._id;
            return t.data.locationId === this._id;
        });
    }

    /* -------------------------------------------- */

    /**
     * Prepare a data object which defines the data schema used by dice roll commands against this Location
     * @return {Object}
     */
    getRollData() {
        return duplicate(this.data.data);
    }

    /* -------------------------------------------- */

    /**
     * Get an Array of Token images which could represent this Location
     * @return {Promise}
     */
    async getTokenImages() {
        if (!this.data.token.randomImg) return [this.data.token.img];
        if (this._tokenImages) return this._tokenImages;
        let source = "data";
        let pattern = this.data.token.img;
        const browseOptions = {wildcard: true};

        // Support S3 matching
        if (/\.s3\./.test(pattern)) {
            source = "s3";
            const {bucket, keyPrefix} = FilePicker.parseS3URL(pattern);
            if (bucket) {
                browseOptions.bucket = bucket;
                pattern = keyPrefix;
            }
        }

        // Retrieve wildcard content
        try {
            const content = await FilePicker.browse(source, pattern, browseOptions);
            this._tokenImages = content.files;
        } catch (err) {
            this._tokenImages = [];
            ui.notifications.error(err);
        }
        return this._tokenImages;
    }

    /* -------------------------------------------- */

    /**
     * Handle how changes to a Token attribute bar are applied to the Location.
     * This allows for game systems to override this behavior and deploy special logic.
     * @param {string} attribute    The attribute path
     * @param {number} value        The target attribute value
     * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
     * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
     * @return {Promise}
     */
    async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
        const current = getProperty(this.data.data, attribute);
        if (isBar) {
            if (isDelta) value = Math.clamped(0, Number(current.value) + value, current.max);
            return this.update({[`data.${attribute}.value`]: value});
        } else {
            if (isDelta) value = Number(current) + value;
            return this.update({[`data.${attribute}`]: value});
        }
    }

    /* -------------------------------------------- */

    /*  Socket Listeners and Handlers
     /* -------------------------------------------- */

    /** @override */
    async update(data, options = {}) {
        if (this.isToken) return LocationTokenHelpers.prototype.update.bind(this)(data, options);

        // Update the default Token image when an Location avatar is assigned
        if (data.img && !hasProperty(data, "token.img")) {
            if (!this.data.token.img || (this.data.token.img === CONST.DEFAULT_TOKEN)) {
                data["token.img"] = data.img;
            }
        }

        // Call the main Entity update logic
        return super.update(data, options);
    }

    /* -------------------------------------------- */

    /** @override */
    async delete(options) {
        if (this.isToken) return this.token.delete(options);
        return super.delete(options);
    }

    /* -------------------------------------------- */

    /** @override */
    async createEmbeddedEntity(...args) {
        if (this.isToken) return LocationTokenHelpers.prototype.createEmbeddedEntity.call(this, ...args);
        return super.createEmbeddedEntity(...args);
    }

    /* -------------------------------------------- */

    /** @override */
    async updateEmbeddedEntity(...args) {
        if (this.isToken) return LocationTokenHelpers.prototype.updateEmbeddedEntity.call(this, ...args);
        return super.updateEmbeddedEntity(...args);
    }

    /* -------------------------------------------- */

    /** @override */
    async deleteEmbeddedEntity(...args) {
        if (this.isToken) return LocationTokenHelpers.prototype.deleteEmbeddedEntity.call(this, ...args);
        return super.deleteEmbeddedEntity(...args);
    }

    /* -------------------------------------------- */

    /** @override */
    _onUpdate(data, options, userId, context) {

        // Get the changed attributes
        const keys = Object.keys(data).filter(k => k !== "_id");
        const changed = new Set(keys);

        // Re-prepare Location data
        if (changed.has("items")) this.prepareEmbeddedEntities();
        this.prepareData();

        // Render associated applications
        this.render(false, context);

        // Additional options only apply to Locations which are not synthetic Tokens
        if (this.isToken) return;

        // Update default token data
        const token = this.data.token;
        if (data.img && data.img !== token.img && (!token.img || token.img === CONST.DEFAULT_TOKEN)) {
            data["token.img"] = data.img;
        }
        if (data.name && data.name !== token.name && (!token.name || token.name === "New Location")) {
            data["token.name"] = data.name;
        }

        // If the prototype token was changed, expire any cached token images
        if (changed.has("token")) this._tokenImages = null;

        // Update Token representations of this Location
        this.getActiveTokens().forEach(token => token._onUpdateBaseLocation(this.data, data));

        // If ownership changed for an location with an active token, re-initialize sight
        if (changed.has("permission")) {
            if (this.getActiveTokens().length) {
                canvas.tokens.releaseAll();
                canvas.tokens.cycleTokens(1, true);
            }
        }
    }

    /* -------------------------------------------- */

    /** @override */
    _onCreateEmbeddedEntity(embeddedName, child, options, userId) {
        const item = Item.createOwned(child, this);
        this.items.set(item.id, item);
        if (options.renderSheet && (userId === game.user._id)) {
            item.sheet.render(true, {
                renderContext: "create" + embeddedName,
                renderData: child
            });
        }
    }

    /* -------------------------------------------- */

    /** @override */
    _onUpdateEmbeddedEntity(embeddedName, child, updateData, options, userId) {
        const item = this.getInnerLocation(child._id);
        item.prepareData();
    }

    /* -------------------------------------------- */

    /** @override */
    _onDeleteEmbeddedEntity(embeddedName, child, options, userId) {
        const item = this.getInnerLocation(child._id);
        this.items.delete(item.id);
        item.sheet.close({submit: false});
    }

    /* -------------------------------------------- */
    /*  Inner Location Management                   */

    /* -------------------------------------------- */

    /**
     * Import a new Inner Location from a compendium collection
     * The imported Item is then added to the Location as an inner location.
     *
     * @param collection {String}     The name of the pack from which to import
     * @param entryId {String}        The ID of the compendium entry to import
     */
    importItemFromCollection(collection, entryId) {
        const pack = game.packs.get(collection);
        if (pack.metadata.entity !== "Item") return;
        return pack.getEntity(entryId).then(ent => {
            console.log(`${vtt} | Importing Item ${ent.name} from ${collection}`);
            delete ent.data._id;
            return this.createInnerLocation(ent.data);
        });
    }

    /* -------------------------------------------- */

    /**
     * Get an Item instance corresponding to the Owned Item with a given id
     * @param {string} itemId   The InnerLocation id to retrieve
     * @return {Item}           An Item instance representing the Owned Item within the Location entity
     */
    getInnerLocation(itemId) {
        return this.items.get(itemId);
    }

    /* -------------------------------------------- */

    /**
     * Create a new item owned by this Location. This redirects its arguments to the createEmbeddedEntity method.
     * @see {Entity#createEmbeddedEntity}
     *
     * @param {Object} itemData     Data for the newly inner location
     * @param {Object} options      Item creation options
     * @param {boolean} options.renderSheet Render the Item sheet for the newly created item data
     * @return {Promise.<Object>}   A Promise resolving to the created Owned Item data
     */
    async createInnerLocation(itemData, options = {}) {
        return this.createEmbeddedEntity("InnerLocation", itemData, options);
    }

    /* -------------------------------------------- */

    /**
     * Update an inner location using provided new data. This redirects its arguments to the updateEmbeddedEntity method.
     * @see {Entity#updateEmbeddedEntity}
     *
     * @param {Object} itemData     Data for the item to update
     * @param {Object} options      Item update options
     * @return {Promise.<Object>}   A Promise resolving to the updated Owned Item data
     */
    async updateInnerLocation(itemData, options = {}) {
        return this.updateEmbeddedEntity("InnerLocation", itemData, options);
    }

    /* -------------------------------------------- */

    /**
     * Delete an inner location by its id. This redirects its arguments to the deleteEmbeddedEntity method.
     * @see {Entity#deleteEmbeddedEntity}
     *
     * @param {string} itemId       The ID of the item to delete
     * @param {Object} options      Item deletion options
     * @return {Promise.<Object>}   A Promise resolving to the deleted Owned Item data
     */
    async deleteInnerLocation(itemId, options = {}) {
        return this.deleteEmbeddedEntity("InnerLocation", itemId, options);
    }
}

