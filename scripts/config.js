export const LOCATION_CONFIG = {};

LOCATION_CONFIG.mod = 'location-entity';
LOCATION_CONFIG.path = `modules/${LOCATION_CONFIG.mod}`;

const pre_log = "🏰 Location Entity 🏰";
const log_info_css = "color: #000080;";
const log_debug_css = "background: #ffc080";
LOCATION_CONFIG.pre_log = pre_log;
LOCATION_CONFIG.log_info_css = log_info_css;
LOCATION_CONFIG.log_debug_css = log_debug_css;

LOCATION_CONFIG.ASCII = "\n" +
    "██╗      ██████╗  ██████╗ █████╗ ████████╗██╗ ██████╗ ███╗   ██╗\n" +
    "██║     ██╔═══██╗██╔════╝██╔══██╗╚══██╔══╝██║██╔═══██╗████╗  ██║\n" +
    "██║     ██║   ██║██║     ███████║   ██║   ██║██║   ██║██╔██╗ ██║\n" +
    "██║     ██║   ██║██║     ██╔══██║   ██║   ██║██║   ██║██║╚██╗██║\n" +
    "███████╗╚██████╔╝╚██████╗██║  ██║   ██║   ██║╚██████╔╝██║ ╚████║\n" +
    "╚══════╝ ╚═════╝  ╚═════╝╚═╝  ╚═╝   ╚═╝   ╚═╝ ╚═════╝ ╚═╝  ╚═══╝\n" +
    "                                                                \n" +
    "        ███████╗███╗   ██╗████████╗██╗████████╗██╗   ██╗        \n" +
    "        ██╔════╝████╗  ██║╚══██╔══╝██║╚══██╔══╝╚██╗ ██╔╝        \n" +
    "        █████╗  ██╔██╗ ██║   ██║   ██║   ██║    ╚████╔╝         \n" +
    "        ██╔══╝  ██║╚██╗██║   ██║   ██║   ██║     ╚██╔╝          \n" +
    "        ███████╗██║ ╚████║   ██║   ██║   ██║      ██║           \n" +
    "        ╚══════╝╚═╝  ╚═══╝   ╚═╝   ╚═╝   ╚═╝      ╚═╝           \n" +
    "                                                                \n";

LOCATION_CONFIG.logger = function () {
//    return Function.prototype.bind.call(console.log, console, `%c${pre_log}|${msg}`, log_info_css, ...args);
    return Function.prototype.bind.call(console.log, console, `%c${pre_log}|\n`, log_info_css);
}();

LOCATION_CONFIG.debug = function () {
//    return Function.prototype.bind.call(console.debug, console, `%c${pre_log}|${msg}`, log_debug_css, ...args);
    return Function.prototype.bind.call(console.debug, console, `%c${pre_log}|\n`, log_debug_css);
}();

LOCATION_CONFIG.zombie = function (obj) {
    return JSON.parse(JSON.stringify(obj));
}
