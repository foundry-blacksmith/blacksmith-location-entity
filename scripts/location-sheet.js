/**
 * The default Location Sheet
 *
 * This Application is responsible for rendering an location's attributes and allowing the location to be edited.
 *
 * System modifications may elect to override this class to better suit their own game system by re-defining the value
 * ``CONFIG.Location.sheetClass``.

 * @type {BaseEntitySheet}
 *
 * @param location {Location}                 The Location instance being displayed within the sheet.
 * @param [options] {Object}            Additional options which modify the rendering of the Location's sheet.
 * @param [options.editable] {Boolean}  Is the Location editable? Default is true.
 */
export class LocationSheet extends BaseEntitySheet {
    constructor(...args) {
        super(...args);

        /**
         * If this Location Sheet represents a synthetic Token location, reference the active Token
         * @type {LocationToken}
         */
        this.token = this.object.token;
    }

    /* -------------------------------------------- */

    /**
     * Default rendering and configuration options used for the LocationSheet and its subclasses.
     * See Application.defaultOptions and FormApplication.defaultOptions for more details.
     * @type {Object}
     */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            height: 720,
            width: 800,
            template: "modules/location-entity/templates/sheets/location-sheet.html",
            closeOnSubmit: false,
            submitOnClose: true,
            submitOnChange: true,
            resizable: true,
            baseApplication: "LocationSheet",
            dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
        });
    }

    /* -------------------------------------------- */

    /**
     * Define a unique and dynamic element ID for the rendered LocationSheet application
     * @return {string}
     */
    get id() {
        const location = this.location;
        let id = `location-${location.id}`;
        if (location.isToken) id += `-${location.token.id}`;
        return id;
    }

    /* -------------------------------------------- */

    /**
     * The displayed window title for the sheet - the entity name by default
     * @type {String}
     */
    get title() {
        return (this.token && !this.token.data.locationLink) ? `[Token] ${this.location.name}` : this.location.name;
    }

    /* -------------------------------------------- */

    /**
     * A convenience reference to the Location entity
     * @type {Location}
     */
    get location() {
        return this.object;
    }

    /* -------------------------------------------- */

    /** @override */
    getData() {
        const data = super.getData();

        // Entity data
        data.location = data.entity;
        data.data = data.entity.data;

        // Owned items
        data.items = data.location.items;
        data.items.sort((a, b) => (a.sort || 0) - (b.sort || 0));
        return data;
    }

    /* -------------------------------------------- */

    /** @override */
    async _render(force = false, options = {}) {
        if (force) this.token = options.token || null;
        return super._render(force, options);
    }

    /* -------------------------------------------- */

    /** @inheritdoc */
    _getHeaderButtons() {
        let buttons = super._getHeaderButtons();

        // Token Configuration
        const canConfigure = game.user.isGM || (this.location.owner && game.user.can("TOKEN_CONFIGURE"));
        if (this.options.editable && canConfigure) {
            buttons = [
                {
                    label: "Sheet",
                    class: "configure-sheet",
                    icon: "fas fa-cog",
                    onclick: ev => this._onConfigureSheet(ev)
                },
                {
                    label: this.token ? "Token" : "Prototype Token",
                    class: "configure-token",
                    icon: "fas fa-user-circle",
                    onclick: ev => this._onConfigureToken(ev)
                }
            ].concat(buttons);
        }
        return buttons;
    }

    /* -------------------------------------------- */
    /*  Event Listeners                             */

    /* -------------------------------------------- */

    /** @override */
    activateListeners(html) {
        super.activateListeners(html);

        // Support Image updates
        if (this.options.editable) {
            html.find('img[data-edit]').click(ev => this._onEditImage(ev));
        }
    }

    /* -------------------------------------------- */

    /**
     * Handle requests to configure the prototype Token for the Location
     * @private
     */
    _onConfigureToken(event) {
        event.preventDefault();

        // Determine the Token for which to configure
        const token = this.token || new LocationToken(this.location.data.token);

        // Render the Token Config application
        new TokenConfig(token, {
            left: Math.max(this.position.left - 560 - 10, 10),
            top: this.position.top,
            configureDefault: !this.token
        }).render(true);
    }

    /* -------------------------------------------- */

    /**
     * Handle requests to configure the default sheet used by this Location
     * @private
     */
    _onConfigureSheet(event) {
        event.preventDefault();
        new EntitySheetConfig(this.location, {
            top: this.position.top + 40,
            left: this.position.left + ((this.position.width - 400) / 2)
        }).render(true);
    }

    /* -------------------------------------------- */

    /**
     * Handle changing the location profile image by opening a FilePicker
     * @private
     */
    _onEditImage(event) {
        const attr = event.currentTarget.dataset.edit;
        const current = getProperty(this.location.data, attr);
        new FilePicker({
            type: "image",
            current: current,
            callback: path => {
                event.currentTarget.src = path;
                this._onSubmit(event);
            },
            top: this.position.top + 40,
            left: this.position.left + 10
        }).browse(current);
    }

    /* -------------------------------------------- */
    /*  Drag and Drop                               */

    /* -------------------------------------------- */

    /** @override */
    _canDragStart(selector) {
        return this.options.editable && this.location.owner;
    }

    /* -------------------------------------------- */

    /** @override */
    _canDragDrop(selector) {
        return this.options.editable && this.location.owner;
    }

    /* -------------------------------------------- */

    /** @override */
    _onDragStart(event) {
        const li = event.currentTarget;
        const item = this.location.getOwnedItem(li.dataset.itemId);
        const dragData = {
            type: "Item",
            locationId: this.location.id,
            data: item.data
        };
        if (this.location.isToken) dragData.tokenId = this.location.token.id;
        event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
    }

    /* -------------------------------------------- */

    /**
     * @deprecated since 0.5.6
     */
    _onDragItemStart(event) {
        return this._onDragStart(event);
    }

    /* -------------------------------------------- */

    /** @override */
    async _onDrop(event) {

        // Try to extract the data
        let data;
        try {
            data = JSON.parse(event.dataTransfer.getData('text/plain'));
            if (data.type !== "Item") return;
        } catch (err) {
            return false;
        }

        // Case 1 - Import from a Compendium pack
        const location = this.location;
        if (data.pack) {
            return location.importItemFromCollection(data.pack, data.id);
        }

        // Case 2 - Data explicitly provided
        else if (data.data) {
            let sameLocation = data.locationId === location._id;
            if (sameLocation && location.isToken) sameLocation = data.tokenId === location.token.id;
            if (sameLocation) return this._onSortItem(event, data.data); // Sort existing items
            else return location.createEmbeddedEntity("OwnedItem", duplicate(data.data));  // Create a new Item
        }

        // Case 3 - Import from World entity
        else {
            let item = game.items.get(data.id);
            if (!item) return;
            return location.createEmbeddedEntity("OwnedItem", duplicate(item.data));
        }
    }

    /* -------------------------------------------- */

    /*  Owned Item Sorting
     /* -------------------------------------------- */

    /**
     * Handle a drop event for an existing Owned Item to sort that item
     * @param {Event} event
     * @param {Object} itemData
     * @private
     */
    _onSortItem(event, itemData) {

        // TODO - for now, don't allow sorting for Token Location ovrrides
        if (this.location.isToken) return;

        // Get the drag source and its siblings
        const source = this.location.getOwnedItem(itemData._id);
        const siblings = this._getSortSiblings(source);

        // Get the drop target
        const dropTarget = event.target.closest(".item");
        const targetId = dropTarget ? dropTarget.dataset.itemId : null;
        const target = siblings.find(s => s.data._id === targetId);

        // Ensure we are only sorting like-types
        if (target && (source.data.type !== target.data.type)) return;

        // Perform the sort
        const sortUpdates = SortingHelpers.performIntegerSort(source, {target: target, siblings});
        const updateData = sortUpdates.map(u => {
            const update = u.update;
            update._id = u.target.data._id;
            return update;
        });

        // Perform the update
        return this.location.updateEmbeddedEntity("OwnedItem", updateData);
    }

    /* -------------------------------------------- */

    _getSortSiblings(source) {
        return this.location.items.filter(i => {
            return (i.data.type === source.data.type) && (i.data._id !== source.data._id);
        });
    }
}
