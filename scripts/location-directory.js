import {Locations} from "./locations.js";
import {LOCATION_CONFIG} from "./config.js";

const path = LOCATION_CONFIG.path;

/**
 * A directory list of Location entities in the Sidebar
 * @type {SidebarDirectory}
 * @see {@link Location}
 */
export class LocationDirectory extends SidebarDirectory {
    constructor(...args) {
        super(...args);
        this._dragDrop[0].permissions["dragstart"] = () => game.user.can("LOCATION_TOKEN_CREATE");
        this._dragDrop[0].permissions["dragdrop"] = () => game.user.can("LOCATION_CREATE");
    }

    /* -------------------------------------------- */

    /** @override */
    static get entity() {
        return "Location";
    }

    /* -------------------------------------------- */

    /** @override */
    static get collection() {
        if (!("locations" in game)) {
            game.locations = new Locations([]);
        }
        return game.locations;
    }

    /* -------------------------------------------- */

    /** @override */
    static get defaultOptions() {
        const el = this.entity.toLowerCase();
        return mergeObject(super.defaultOptions, {
            id: "locations",
            template: `${path}/templates/sidebar/location-directory.html`,
            title: `${this.entity}s Directory`,
            renderUpdateKeys: ["name", "img", "thumb", "permission", "sort", "folder"],
            height: "auto",
            scrollY: ["ol.directory-list"],
            dragDrop: [{dragSelector: ".directory-item", dropSelector: ".directory-list"}]
        });
    }


    /** @override */
    getData(options) {
        const data = super.getData(options);
        data.folderPartial = "templates/sidebar/folder-partial.html";
        data.entityPartial = "modules/location-entity/templates/sidebar/location-partial.html";

        return data;
    }

    /* -------------------------------------------- */

    /** @override */
    _canDragStart(selector) {
        return game.user.can("LOCATION_TOKEN_CREATE");
    }

    /* -------------------------------------------- */

    /** @override */
    _onDragStart(event) {
        super._onDragStart(event);
        const li = event.currentTarget.closest(".directory-item");
        const location = game.locations.get(li.dataset.entityId);

        // Create the drag preview for the Token
        if (canvas.ready) {
            const img = li.querySelector("img");
            const td = location.data.token;
            const w = td.width * canvas.dimensions.size * td.scale * canvas.stage.scale.x;
            const h = td.height * canvas.dimensions.size * td.scale * canvas.stage.scale.y;
            const preview = DragDrop.createDragImage(img, w, h);
            event.dataTransfer.setDragImage(preview, w / 2, h / 2);
        }
    }

    /* -------------------------------------------- */

    /** @override */
    _canDragDrop(selector) {
        return game.user.can("LOCATION_CREATE");
    }

    /* -------------------------------------------- */

    /** @override */
    async _onClickEntityName(event) {
        event.preventDefault();
        const locationId = event.currentTarget.closest(".location").dataset.entityId;
        const location = this.constructor.collection.get(locationId);
        const sheet = location.sheet;
        if (location.sheet.token) await location.sheet.close();
        else if (sheet._minimized) return sheet.maximize();
        return sheet.render(true);
    }

    /* -------------------------------------------- */

    /** @override */
    _getEntryContextOptions() {
        const options = super._getEntryContextOptions();
        return [
            {
                name: "SIDEBAR.CharArt",
                icon: '<i class="fas fa-image"></i>',
                condition: li => {
                    const location = game.locations.get(li.data("entityId"));
                    return location.data.img !== CONST.DEFAULT_TOKEN;
                },
                callback: li => {
                    const location = game.locations.get(li.data("entityId"));
                    new ImagePopout(location.data.img, {
                        title: location.name,
                        shareable: true,
                        uuid: location.uuid
                    }).render(true);
                }
            },
            {
                name: "SIDEBAR.TokenArt",
                icon: '<i class="fas fa-image"></i>',
                condition: li => {
                    const location = game.locations.get(li.data("entityId"));
                    if (location.data.token.randomImg) return false;
                    return ![null, undefined, CONST.DEFAULT_TOKEN].includes(location.data.token.img);
                },
                callback: li => {
                    const location = game.locations.get(li.data("entityId"));
                    new ImagePopout(location.data.token.img, {
                        title: location.name,
                        shareable: true,
                        uuid: location.uuid
                    }).render(true);
                }
            }
        ].concat(options);
    }
}
