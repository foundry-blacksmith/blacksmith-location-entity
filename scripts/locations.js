import {Location} from "./location-entity.js";

export class Locations extends EntityCollection {
    constructor(...args) {
        super(...args);

        /**
         * A mapping of synthetic Token Locations which are currently active within the viewed Scene.
         * Each Location is referenced by the Token.id.
         * @type {Object}
         */
        this.tokens = {};
    }

    /* -------------------------------------------- */

    /** @override */
    get object() {
        return CONFIG.Location.entityClass;
    }

    /* -------------------------------------------- */
    /*  Sheet Registration Methods                  */

    /* -------------------------------------------- */

    /**
     * Register an Location sheet class as a candidate which can be used to display Locations of a given type
     * See EntitySheetConfig.registerSheet for details
     * @static
     *
     * @example <caption>Register a new LocationSheet subclass for use with certain Location types.</caption>
     * Locations.registerSheet("dnd5e", LocationSheet5eCharacter, { types: ["character"], makeDefault: true });
     */
    static registerSheet(...args) {
        EntitySheetConfig.registerSheet(Location, ...args);
    }

    /* -------------------------------------------- */

    /**
     * Unregister an Location sheet class, removing it from the list of avaliable sheet Applications to use
     * See EntitySheetConfig.unregisterSheet for details
     * @static
     *
     * @example <caption>Deregister the default LocationSheet subclass to replace it with others.</caption>
     * Locations.unregisterSheet("core", LocationSheet);
     */
    static unregisterSheet(...args) {
        EntitySheetConfig.unregisterSheet(Location, ...args);
    }

    /* -------------------------------------------- */

    /**
     * Return an Array of currently registered sheet classes for this Entity type
     * @type {LocationSheet[]}
     */
    static get registeredSheets() {
        const sheets = new Set();
        for (let t of Object.values(CONFIG.Location.sheetClasses)) {
            for (let s of Object.values(t)) {
                sheets.add(s.cls);
            }
        }
        return Array.from(sheets);
    }

}
